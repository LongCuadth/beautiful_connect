import 'package:json_annotation/json_annotation.dart';

part 'banner_model.g.dart';

@JsonSerializable(
  createToJson: true,
)
class BannerModel {
  int? id;
  String? name;
  String? image;
  int? type;

  BannerModel({this.id, this.name, this.type, this.image});

  factory BannerModel.fromJson(Map<String, dynamic> json) =>
      _$BannerModelFromJson(json);

  Map<String, dynamic> toJson() => _$BannerModelToJson(this);
}
