import 'package:app/common/app_parameters.dart';
import 'package:app/data/models/booking_model.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrderFinalController extends GetxController {
  var bookingModel = BookingModel().obs;
  var textEditing = TextEditingController();
  var isShowButton = true.obs;
  var servicesString = ''.obs;
  var totalDuration = 0.0.obs;

  @override
  void onInit() {
    if (Get.arguments != null &&
        Get.arguments[AppParameters.bookingModel] != null) {
      bookingModel.value = Get.arguments[AppParameters.bookingModel];
      _calculateDuration();
      _getServicesSelect();
    }
    textEditing.text = 'Linda';
    super.onInit();
  }

  order() {
    if (textEditing.text.isEmpty) {
      Get.find<MessageService>().send(Message.error(content: 'empty_name'.tr));
      return;
    }
    Get.offAllNamed(Routes.MAIN_VIEW);
    Get.find<MessageService>()
        .send(Message.success(content: 'order_success'.tr));
  }

  void _getServicesSelect() {
    // if (bookingModel.value.servicesModel?.isEmpty == true) return;
    // if (bookingModel.value.servicesModel?.length == 1) {
    //   servicesString.value = bookingModel.value.servicesModel?.first.name ?? '';
    // } else {
    //   servicesString.value = bookingModel.value.servicesModel
    //           ?.map((e) => e.name)
    //           .toList()
    //           .join(', ') ??
    //       '';
    // }
  }

  void _calculateDuration() {
    // bookingModel.value.servicesModel?.forEach((element) {
    //   totalDuration.value += double.parse(element.duration ?? "0.0");
    // });
  }
}
