import 'package:app/base/widgets/base_appbar.dart';
import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/validator_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeProvider.colorBackgroundScreen,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            _buildFormLogin(),
            Positioned(
                bottom: 40,
                child: SizedBox(
                    width: Get.width,
                    height: 40,
                    child: Center(
                        child: GestureDetector(
                      onTap: () {
                        Get.bottomSheet(Scaffold(
                          appBar: BaseAppBar(
                            isShowBackButton: false,
                            onBackPress: () {
                              Get.back();
                            },
                            title: 'policy_header'.tr,
                          ),
                          body: const SizedBox.shrink(),
                        ));
                      },
                      child: Text(
                        'policy'.tr,
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontFamily: ThemeProvider.fontLight,
                            fontSize: ThemeProvider.fontSize14,
                            color: ThemeProvider.colorPolicy),
                      ),
                    ))))
          ],
        ),
      ),
    );
  }

  Widget _buildFormLogin() {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 16.w),
      child: Form(
        key: controller.keyForm,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 80.h),
            Align(
              alignment: Alignment.center,
              child: Text(
                'app_name'.tr,
                style: TextStyle(
                    fontFamily: ThemeProvider.fontLogoBold,
                    fontSize: ThemeProvider.fontSize28),
              ),
            ),
            SizedBox(height: 100.h),
            BaseTextField(
                    validator: (value) {
                      return ValidatorUtils.validatorEmail(value,
                          isRequired: true);
                    },
                    inputType: TextInputType.text,
                    textEditingController: controller.emailController,
                    hintText: 'email'.tr)
                .marginSymmetric(horizontal: 16.w),
            SizedBox(height: 25.h),
            BaseTextField(
                    validator: (value) {
                      return ValidatorUtils.validatorPass(value);
                    },
                    isPassword: true,
                    textEditingController: controller.passController,
                    hintText: 'password'.tr)
                .marginSymmetric(horizontal: 16.w),
            SizedBox(height: 40.h),
            BaseButton(
                radius: 4,
                title: 'login'.tr,
                onPressed: () {
                  controller.login();
                },
                styleButton: BaseButtonStyle.fill),
            SizedBox(height: 20.h),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              BaseButton(
                  width: 100,
                  radius: 4,
                  title: 'register'.tr,
                  onPressed: () {
                    Get.toNamed(Routes.REGISTER);
                  },
                  styleButton: BaseButtonStyle.textOnly),
              BaseButton(
                  width: 100,
                  radius: 4,
                  title: 'forgot_pass'.tr,
                  onPressed: () {
                    Get.toNamed(Routes.FORGOT_PASSWORD);
                  },
                  styleButton: BaseButtonStyle.textOnly),
            ]),
            SizedBox(height: 20.h),
          ],
        ),
      ),
    );
  }
}
