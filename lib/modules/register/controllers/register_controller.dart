import 'package:app/data/providers/auth/auth_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class RegisterController extends GetxController {
  var formKey = GlobalKey<FormState>();
  var phoneController = TextEditingController();
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var passController = TextEditingController();
  var checkPassController = TextEditingController();
  var inviteController = TextEditingController();
  var avatarPath = ''.obs;
  var isSendedEmail = false.obs;
  var messageService = Get.find<MessageService>();
  final IAuthProvider _authProvider = Get.find<IAuthProvider>();

  registerUser() async {
    EasyLoading.show();
    //GERMAN01
    var response = await _authProvider.registerUser({
      'email': emailController.text,
      'phone': phoneController.text,
      'password': passController.text,
      'shop_code': inviteController.text,
    });
    if (response.success == true) {
      messageService.send(Message.success(content: 'register_success'.tr));
      Get.back();
    } else {
      messageService.send(Message.error(content: response.message ?? ''));
    }
    EasyLoading.dismiss();
  }
}
