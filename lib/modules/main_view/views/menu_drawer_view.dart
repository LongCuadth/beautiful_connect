import 'package:app/common/network/shared_preference.dart';
import 'package:app/modules/main_view/controllers/main_view_controller.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class MenuDrawerView extends GetWidget<MainViewController> {
  final MyDrawerController drawerController;
  const MenuDrawerView({super.key, required this.drawerController});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        drawerController.toggleDrawer();
      },
      child: Container(
        padding: const EdgeInsets.all(20),
        // color: ThemeProvider.q.withOpacity(0.05),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Icon(
                Icons.close,
                size: 30,
              ).marginOnly(left: Get.width * 0.65 - 50.w, bottom: 30.h),
              _buildItemMenu(Icons.person_sharp, 'user_setting'.tr, () {}),
              _buildItemMenu(Icons.business_outlined, 'info_shop'.tr, () {}),
              _buildItemMenu(Icons.history, 'history_booking'.tr, () {}),
              _buildItemMenu(
                  Icons.shield_outlined, 'change_password'.tr, () {}),
              _buildItemMenu(Icons.logout, 'logout'.tr, () async {
                await SharedPreference.shared
                    .remove(SharedPreferenceKey.tokenApiKey);
                await SharedPreference.shared
                    .remove(SharedPreferenceKey.userModelKey);
                await SharedPreference.shared
                    .remove(SharedPreferenceKey.shopModelKey);
                Get.offAllNamed(Routes.LOGIN);
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildItemMenu(IconData icon, String action, VoidCallback ontap) {
    return InkWell(
      onTap: () {
        ontap.call();
      },
      child: Container(
        padding: const EdgeInsets.all(8),
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Icon(
            icon,
            color: ThemeProvider.colorPrimary,
          ).marginOnly(right: 12.w),
          Text(
            action,
            style: TextStyle(
                color: ThemeProvider.colorBlack,
                fontFamily: ThemeProvider.fontRegular,
                fontSize: ThemeProvider.fontSize16),
          )
        ]).marginOnly(bottom: 20.h),
      ),
    );
  }
}
