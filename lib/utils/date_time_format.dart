String formatMinuteToString(double minutes) {
  int hours = minutes ~/ 60;
  int remainingMinutes = (minutes % 60).round();

  if (hours == 0) {
    return '$remainingMinutes mins';
  } else if (remainingMinutes == 0) {
    return '${hours}h';
  } else {
    return '${hours}h$remainingMinutes mins';
  }
}
