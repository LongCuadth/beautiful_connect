// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const SPLASH = _Paths.SPLASH;
  static const HOME = _Paths.HOME;
  static const CATEGORY = _Paths.CATEGORY;
  static const NOTIFICATION = _Paths.NOTIFICATION;
  static const ACCOUNT = _Paths.ACCOUNT;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const FORGOT_PASSWORD = _Paths.FORGOT_PASSWORD;
  static const VERIFY_OTP = _Paths.VERIFY_OTP;
  static const REGISTER_INFO = _Paths.REGISTER_INFO;
  static const MAIN_VIEW = _Paths.MAIN_VIEW;
  static const SELECT_STEP_TIME = _Paths.SELECT_STEP_TIME;
  static const ORDER_FINAL = _Paths.ORDER_FINAL;
}

abstract class _Paths {
  _Paths._();
  static const SPLASH = '/splash';
  static const HOME = '/home';
  static const CATEGORY = '/category';
  static const NOTIFICATION = '/notification';
  static const ACCOUNT = '/account';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const FORGOT_PASSWORD = '/forgot-password';
  static const VERIFY_OTP = '/verify-otp';
  static const REGISTER_INFO = '/register-info';
  static const MAIN_VIEW = '/main-view';
  static const SELECT_STEP_TIME = '/select-step-time';
  static const ORDER_FINAL = '/order-final';
}
