import '../../common/network/http_client.dart';

abstract class BaseProvider {
  final HttpClient httpClient = HttpClient();
}
