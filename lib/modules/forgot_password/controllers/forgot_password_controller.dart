import 'package:app/data/services/message_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgotPasswordController extends GetxController {
  var phoneController = TextEditingController();
  var newPassController = TextEditingController();
  var checkPassController = TextEditingController();
  var isPasswordVerified = false.obs;
  var keyForm = GlobalKey<FormState>();
  var messageService = Get.find<MessageService>();

  Future<void> checkPhone() async {
    if (keyForm.currentState!.validate()) {
      isPasswordVerified.value = true;
    }
    messageService.send(Message.error(content: 'phone_not_exits'.tr));
  }

  void changePassword() {
    if (keyForm.currentState!.validate()) {
      isPasswordVerified.value = false;
      messageService.send(Message.error(content: 'change_password_success'.tr));
    }
  }
}
