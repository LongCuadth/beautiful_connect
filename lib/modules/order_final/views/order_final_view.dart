import 'package:app/gen/assets.gen.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/date_time_format.dart';
import 'package:app/utils/icon_circle_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_detection/keyboard_detection.dart';

import '../controllers/order_final_controller.dart';

class OrderFinalView extends GetView<OrderFinalController> {
  const OrderFinalView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KeyboardDetection(
      controller: KeyboardDetectionController(
        onChanged: (value) {
          if (value == KeyboardState.hidden) {
            controller.isShowButton.value = true;
          } else {
            controller.isShowButton.value = false;
          }
        },
      ),
      child: Scaffold(
          // resizeToAvoidBottomInset: false,
          backgroundColor: ThemeProvider.colorBackgroundScreen,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: ThemeProvider.colorBackgroundScreen,
            leading: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: const Icon(
                  Icons.arrow_back_ios,
                  color: ThemeProvider.colorBlack,
                )),
            title: Text(
              'app_name'.tr,
              style: TextStyle(
                  color: ThemeProvider.colorBlack,
                  fontFamily: ThemeProvider.fontLogoBold,
                  fontSize: ThemeProvider.fontSize28),
            ),
            centerTitle: true,
          ),
          body: Stack(
            children: [
              SizedBox(
                width: Get.width,
                height: Get.height,
                child: SingleChildScrollView(
                  controller: ScrollController(),
                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 20.h),
                        Row(
                          children: [
                            Text(
                              'booking_info'.tr,
                              style: TextStyle(
                                  fontFamily: ThemeProvider.fontMedium,
                                  fontSize: ThemeProvider.fontSize14),
                            ),
                            SizedBox(width: 12.w),
                            Assets.svg.icLock.svg(),
                          ],
                        ),
                        SizedBox(height: 40.h),
                        _buildCardOrder(),
                        SizedBox(height: 20.h),
                        _buildUserOrder(),
                        SizedBox(height: 140.h),
                      ]),
                ),
              ),
              _buildButtonBottom()
            ],
          )),
    );
  }

  Widget _buildButtonBottom() {
    return Obx(() {
      return Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Visibility(
            visible: controller.isShowButton.value,
            child: Container(
              decoration: const BoxDecoration(
                  color: ThemeProvider.colorBackgroundScreen,
                  border: Border(
                      top: BorderSide(
                          color: ThemeProvider.bgButtonFill, width: 0.5))),
              width: Get.width,
              height: 80,
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 24.w, vertical: 16.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('${controller.bookingModel.value.listProduct?.length} ${'services'.tr}',
                                style: TextStyle(
                                    fontFamily: ThemeProvider.fontMedium,
                                    fontSize: ThemeProvider.fontSize16))
                            .marginOnly(bottom: 5.h),
                        Text('${controller.bookingModel.value.price} \$',
                            style: TextStyle(
                                fontFamily: ThemeProvider.fontRegular,
                                fontSize: ThemeProvider.fontSize14))
                      ],
                    ),
                  )),
                  Expanded(
                      child: InkWell(
                    onTap: () {
                      controller.order();
                    },
                    child: Container(
                      color: Colors.red,
                      child: Center(
                          child: Text(
                        'order'.tr,
                        style: TextStyle(
                            fontFamily: ThemeProvider.fontMedium,
                            fontSize: ThemeProvider.fontSize16,
                            color: ThemeProvider.colorWhite),
                      )),
                    ),
                  ))
                ],
              ),
            ),
          ));
    });
  }

  Widget _buildCardOrder() {
    return Obx(
      () => Container(
        decoration: BoxDecoration(
            color: ThemeProvider.colorWhite,
            // border: Border.all(color: const Color(0xffCECECE)),
            boxShadow: [
              BoxShadow(
                color: ThemeProvider.colorShadowBox.withOpacity(1),
                blurRadius: 20,
                offset: const Offset(0, 3), // changes position of shadow
              ),
            ]),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    flex: 6,
                    child: Container(
                      height: 5.h,
                      color: Colors.red,
                    )),
                Expanded(
                    flex: 1,
                    child: Container(
                      height: 5.h,
                      color: ThemeProvider.colorSelect.withOpacity(0.8),
                    )),
                Expanded(
                    flex: 1,
                    child: Container(
                      height: 5.h,
                      color: ThemeProvider.colorBlack,
                    )),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconCircleWidget(
                  backgroundColor: const Color(0xffE6F0FC),
                  child: Assets.svg.icStore.svg(),
                ).marginOnly(right: 8.w),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'SHOP NAME',
                        style: TextStyle(fontFamily: ThemeProvider.fontBold),
                      ).marginOnly(bottom: 10.h),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Text(
                          //   '${DateFormat('EEE, d MMM').format(controller.bookingModel.value.startDate ?? DateTime.now())} ${controller.bookingModel.value.timeStart?.time}',
                          //   style: const TextStyle(
                          //       fontFamily: ThemeProvider.fontRegular,
                          //       color: ThemeProvider.colorTitleGrey),
                          // ).marginOnly(right: 8.0),
                          GestureDetector(
                              onTap: () {
                                Get.back();
                              },
                              child: Assets.svg.icEdit.svg())
                        ],
                      )
                    ],
                  ),
                )
              ],
            ).paddingAll(16),
            Container(
              child:
                  Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                IconCircleWidget(
                  backgroundColor: const Color(0xffE6FAF8),
                  child: Assets.svg.icServices.svg(),
                ).marginOnly(right: 8.w),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(controller.servicesString.value,
                          style: const TextStyle(
                            fontFamily: ThemeProvider.fontBold,
                          )).marginOnly(bottom: 5.h),
                      Obx(() => Text(
                          formatMinuteToString(controller.totalDuration.value),
                          style: TextStyle(
                              fontFamily: ThemeProvider.fontRegular,
                              fontSize: ThemeProvider.fontSize12,
                              color: ThemeProvider.colorSelect))),
                    ],
                  ),
                )
              ]).paddingAll(16),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconCircleWidget(
                  backgroundColor: const Color(0xffF8F0CC),
                  child: Assets.svg.icUser.svg(),
                ).marginOnly(right: 8.w),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Text(
                      //   controller.bookingModel.value.employeeModel?.name ??
                      //       'auto_select_employee'.tr,
                      //   style: const TextStyle(
                      //       fontFamily: ThemeProvider.fontRegular,
                      //       color: ThemeProvider.colorTitleGrey),
                      // ).marginOnly(bottom: 10.h),
                    ],
                  ),
                )
              ],
            ).paddingAll(16),
          ],
        ),
      ),
    );
  }

  Widget _buildUserOrder() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: ThemeProvider.colorWhite,
        border: Border.all(color: const Color(0xffCECECE)),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'data'.tr,
                style: const TextStyle(fontFamily: ThemeProvider.fontBold),
              ),
              RichText(
                  text: TextSpan(
                      text: '• ',
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontRegular,
                          color: ThemeProvider.colorErrorText,
                          fontSize: ThemeProvider.fontSize16),
                      children: [
                    TextSpan(
                        text: 'required'.tr,
                        style: TextStyle(
                            color: ThemeProvider.colorBlack,
                            fontFamily: ThemeProvider.fontRegular,
                            fontSize: ThemeProvider.fontSize14))
                  ])).marginOnly(right: 16.w)
            ],
          ).marginOnly(bottom: 10.h),
          Row(
            children: [
              Assets.svg.icWelcome.svg().marginOnly(right: 10.w),
              Flexible(
                child: Text(
                  '${'welcome'.tr} username ${'return_back'.tr}',
                  style: const TextStyle(
                    fontFamily: ThemeProvider.fontRegular,
                  ),
                ).marginOnly(right: 10.w),
              ),
              Text(
                'logout'.tr,
                style: const TextStyle(
                    fontFamily: ThemeProvider.fontBold,
                    color: ThemeProvider.colorBlack),
              )
            ],
          ).marginOnly(bottom: 20.h),
          Text(
            'username'.tr,
            style: const TextStyle(
                fontFamily: ThemeProvider.fontBold,
                color: ThemeProvider.colorBlack),
          ).marginOnly(bottom: 10.h),
          Form(
            child: TextFormField(
              readOnly: true,
              controller: controller.textEditing,
              style: TextStyle(
                  fontSize: ThemeProvider.fontSize14,
                  fontFamily: ThemeProvider.fontRegular,
                  color: ThemeProvider.colorBlack),
              decoration: InputDecoration(
                  hintText: 'hint_username'.tr,
                  hintStyle: TextStyle(
                    fontSize: ThemeProvider.fontSize14,
                    fontFamily: ThemeProvider.fontRegular,
                  ),
                  isDense: true,
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 8.w, vertical: 10.h),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: ThemeProvider.colorBlack),
                      borderRadius: BorderRadius.circular(8)),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                  constraints:
                      BoxConstraints(maxHeight: 60.h, minHeight: 40.h)),
            ),
          )
        ],
      ),
    );
  }
}
