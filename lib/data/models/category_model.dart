import 'package:json_annotation/json_annotation.dart';

part 'category_model.g.dart';

@JsonSerializable(
  createToJson: true,
)
class CategoryModel {
  int? id;
  String? name;
  String? image;
  @JsonKey(includeToJson: false)
  bool isSelected;

  CategoryModel({this.id, this.name, this.isSelected = false, this.image});

  factory CategoryModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryModelFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryModelToJson(this);
}
