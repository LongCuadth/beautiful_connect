import 'package:app/common/network/config.dart';
import 'package:app/common/network/shared_preference.dart';
import 'package:app/data/models/banner_model.dart';
import 'package:app/data/models/base/base_list_model.dart';
import 'package:app/data/models/base/base_model.dart';

import 'package:app/data/providers/base_provider.dart';

abstract class IBannerProvider {
  Future<BaseListModel<BannerModel>> getListBanner(
      Map<String, dynamic> payload);
  Future<BaseModel> createBanner(Map<String, dynamic> payload);
  Future<BaseModel> deleteBanner(int idBanner);
}

class BannerProvider extends BaseProvider implements IBannerProvider {
  @override
  Future<BaseModel> createBanner(Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response = await httpClient.sendPostRequestWithFile(
        '/api/${shop?.id}/banner', payload, payload);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(response, (jsonModel) {
      return [];
    });
  }

  @override
  Future<BaseListModel<BannerModel>> getListBanner(
      Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response = await httpClient.sendGetRequest(
        '/api/${shop?.id}/banner', payload, null);
    if (response == null) return BaseListModel(success: false);
    return BaseListModel.fromJson(response, (jsonModel) {
      if (jsonModel.isNotEmpty) {
        return BannerModel.fromJson(jsonModel);
      }
      return BannerModel();
    });
  }

  @override
  Future<BaseModel> deleteBanner(int isEmployee) async {
    var response = await httpClient.sendDeleteRequest(
        ApiPath.deleteBanner + isEmployee.toString(), null);
    return BaseModel.fromJson(response, (jsonModel) {});
  }
}
