import 'package:app/common/app_parameters.dart';
import 'package:app/data/models/booking_model.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/routes/app_pages.dart';
import 'package:flutter_calendar_week/flutter_calendar_week.dart';
import 'package:get/get.dart';

class SelectStepTimeController extends GetxController {
  var bookingModel = Rx<BookingModel?>(null);
  var employeeSelected = Rx<String?>(null);
  var calenderController = CalendarWeekController();
  var seletedDate = DateTime.now().obs;

  // var listTimeSelect = <TimeOrderModel>[
  //   // TimeOrderModel(id: '0', time: '14:00'),
  //   // TimeOrderModel(id: '1', time: '14:30'),
  //   // TimeOrderModel(id: '2', time: '15:00'),
  //   // TimeOrderModel(id: '3', time: '15:30'),
  //   // TimeOrderModel(id: '4', time: '16:00'),
  //   // TimeOrderModel(id: '5', time: '16:30'),
  //   // TimeOrderModel(id: '6', time: '17:00'),
  //   // TimeOrderModel(id: '7', time: '17:30'),
  // ].obs;

  // var seletedTime = Rx<TimeOrderModel?>(null);
  final messageService = Get.find<MessageService>();

  @override
  void onInit() {
    if (Get.arguments != null &&
        Get.arguments[AppParameters.bookingModel] != null) {
      bookingModel.value = Get.arguments[AppParameters.bookingModel];
    }

    super.onInit();
  }

  void order() {
    // if (employeeSelected.value == null) {
    //   messageService.send(Message.error(content: 'please_select_employee'.tr));
    //   return;
    // }

    // if (seletedTime.value == null) {
    //   messageService.send(Message.error(content: 'please_select_datetime'.tr));
    //   return;
    // }

    // bookingModel.value?.timeStart = seletedTime.value;
    // bookingModel.value?.startDate = seletedDate.value;

    Get.toNamed(Routes.ORDER_FINAL, arguments: {
      AppParameters.bookingModel: bookingModel.value,
    });
  }
}
