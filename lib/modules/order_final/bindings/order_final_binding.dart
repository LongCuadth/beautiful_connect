import 'package:get/get.dart';

import '../controllers/order_final_controller.dart';

class OrderFinalBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OrderFinalController>(
      () => OrderFinalController(),
    );
  }
}
