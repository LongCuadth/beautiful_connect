# Beautiful Connect

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

INFO PROJECT
[✓] Flutter (Channel stable, 3.13.9, on macOS 14.0 23A344 darwin-arm64 (Rosetta), locale en-VN)
    • Flutter version 3.13.9 on channel stable at /Users/duclong/fvm/3.13.9
    • Upstream repository https://github.com/flutter/flutter.git
    • Framework revision d211f42860 (3 weeks ago), 2023-10-25 13:42:25 -0700
    • Engine revision 0545f8705d
    • Dart version 3.1.5
    • DevTools version 2.25.0

[✓] Android toolchain - develop for Android devices (Android SDK version 34.0.0)
    • Android SDK at /Users/duclong/Library/Android/sdk
    • Platform android-34, build-tools 34.0.0
    • Java binary at: /Applications/Android Studio.app/Contents/jbr/Contents/Home/bin/java
    • Java version OpenJDK Runtime Environment (build 17.0.6+0-17.0.6b802.4-9586694)
    • All Android licenses accepted.

[✓] Xcode - develop for iOS and macOS (Xcode 15.0)
    • Xcode at /Applications/Xcode.app/Contents/Developer
    • Build 15A240d
    • CocoaPods version 1.14.2

[✓] Chrome - develop for the web
    • Chrome at /Applications/Google Chrome.app/Contents/MacOS/Google Chrome

[✓] Android Studio (version 2022.2)
    • Android Studio at /Applications/Android Studio.app/Contents
    • Flutter plugin can be installed from:
      🔨 https://plugins.jetbrains.com/plugin/9212-flutter
    • Dart plugin can be installed from:
      🔨 https://plugins.jetbrains.com/plugin/6351-dart
    • Java version OpenJDK Runtime Environment (build 17.0.6+0-17.0.6b802.4-9586694)

[✓] VS Code (version 1.84.0)
    • VS Code at /Applications/Visual Studio Code.app/Contents
    • Flutter extension can be installed from:
      🔨 https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter

==================
START CODING - PROJECT USE GET-X , GET CLI
- run command terminal
"fvm flutter pub get && flutter pub run build_runner build --delete-conflicting-outputs"
- create new screen 
"get create page:new_sceen_name  on modules "

