class AppParameters {
  static const String selectedServices = 'selectedServices';
  static const String listProcessSelected = 'listProcessSelected';
  static const String bookingModel = 'bookingModel';
  static String phoneNumber = 'phone_number';
  static String password = 'password';
  static String username = 'username';
}
