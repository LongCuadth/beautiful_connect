import 'package:get/get.dart';

import '../controllers/select_step_time_controller.dart';

class SelectStepTimeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectStepTimeController>(
      () => SelectStepTimeController(),
    );
  }
}
