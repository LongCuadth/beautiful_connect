import 'package:app/flavors.dart';

class NetworkConfig {
  static String get baseAPI => BuildConfig.baseDomain;
}

class ApiPath {
  // AUTH
  static String apiLogin = '/api/login';
  static String checkphone = '/api/auth/check-phone';
  static String apiGetUserInfo = "/api/me";
  static String apiLogout = "/api/logout";
  static String apiRefreshToken = '/api/refresh';
  static String apiUpdateShop = '/api/shop/';
  static String apiGetShopInfo = '/api/shop/';
  static String registerUser = '/api/register';

  // Category
  static String deleteCategory = '/api/service/';
  static String updateCategory = '/api/service/';

  // Service
  static String deleteService = '/api/product/';
  static String updateService = '/api/product/';

  // Employee
  static String deleteEmployee = '/api/user/';
  static String updateEmployee = '/api/user/';

  // Customer
  static String deleteCustomer = '/api/user/';
  static String updateCustomer = '/api/user/';

  // Booking
  static String deleteBooking = '/api/order/';
  static String updateBooking = '/api/order/';
  static String getListBooking = '/api/order/';
  // Banner
  static String deleteBanner = '/api/banner/';
}
