import 'package:app/base/widgets/base_dropdown_button.dart';
import 'package:app/common/network/extension.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_week/flutter_calendar_week.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../controllers/select_step_time_controller.dart';

class SelectStepTimeView extends GetView<SelectStepTimeController> {
  const SelectStepTimeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ThemeProvider.colorBackgroundScreen,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: ThemeProvider.colorBlack,
              )),
          title: Text(
            'app_name'.tr,
            style: TextStyle(
                color: ThemeProvider.colorBlack,
                fontFamily: ThemeProvider.fontLogoBold,
                fontSize: ThemeProvider.fontSize28),
          ),
          centerTitle: true,
        ),
        body: Stack(
          children: [
            SizedBox(
              width: Get.width,
              height: Get.height,
              child: SingleChildScrollView(
                padding: EdgeInsets.only(left: 16.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.h),
                    Text(
                      'select_employee'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontSemiBold,
                          fontSize: ThemeProvider.fontSize16),
                    ),
                    SizedBox(height: 10.h),
                    _buildSelectEmployee(),
                    SizedBox(height: 20.h),
                    Text(
                      'select_date_time'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontSemiBold,
                          fontSize: ThemeProvider.fontSize16),
                    ),
                    SizedBox(height: 10.h),
                    _buildCalendar(),
                    SizedBox(height: 20.h),
                    // _buildListTime(),
                    SizedBox(height: 200.h),
                  ],
                ),
              ),
            ),
            _buildButtonBottom()
          ],
        ));
  }

  Widget _buildCalendar() {
    return Obx(
      () => Container(
        margin: EdgeInsets.only(right: 10.w),
        padding: EdgeInsets.symmetric(vertical: 8.h),
        decoration: BoxDecoration(
            color: ThemeProvider.colorWhite,
            border: Border.all(color: const Color(0xffCECECE)),
            boxShadow: [
              BoxShadow(
                color: ThemeProvider.colorShadowBox.withOpacity(1),
                blurRadius: 10,
                offset: const Offset(0, 3), // changes position of shadow
              ),
            ]),
        child: CalendarWeek(
          dayItemPadding: const EdgeInsets.all(6),
          marginDayOfWeek: EdgeInsets.zero,
          controller: controller.calenderController,
          showMonth: true,
          weekendsIndexes: const [5, 6],
          minDate: DateTime.now()
              .subtract(Duration(days: DateTime.now().weekday - 2))
              .add(
                const Duration(days: -365),
              ),
          maxDate: DateTime.now().add(
            const Duration(days: 365),
          ),
          todayDateStyle: TextStyle(
              fontSize: ThemeProvider.fontSize16,
              color: controller.seletedDate.value.isSameDate(DateTime.now())
                  ? ThemeProvider.colorWhite
                  : ThemeProvider.colorBlack,
              fontFamily: ThemeProvider.fontRegular),
          todayBackgroundColor:
              controller.seletedDate.value.isSameDate(DateTime.now())
                  ? ThemeProvider.colorSelect
                  : Colors.transparent,
          dateBackgroundColor: Colors.transparent,
          pressedDateBackgroundColor: ThemeProvider.colorSelect,
          pressedDateStyle: TextStyle(
              fontSize: ThemeProvider.fontSize16,
              color: ThemeProvider.colorWhite,
              fontFamily: ThemeProvider.fontRegular),
          dateStyle: TextStyle(
              fontSize: ThemeProvider.fontSize16,
              color: ThemeProvider.colorBlack,
              fontFamily: ThemeProvider.fontRegular),
          dayOfWeekStyle: TextStyle(
              fontSize: ThemeProvider.fontSize16,
              color: ThemeProvider.colorBlack,
              fontFamily: ThemeProvider.fontRegular),
          weekendsStyle: TextStyle(
              fontSize: ThemeProvider.fontSize16,
              color: ThemeProvider.colorBlack,
              fontFamily: ThemeProvider.fontRegular),
          onDatePressed: (DateTime datetime) {
            controller.seletedDate.value = datetime;
          },
          onDateLongPressed: (DateTime datetime) {
            // Do something
          },
          onWeekChanged: () {
            // Do something
          },
          monthViewBuilder: (DateTime time) => Align(
            alignment: FractionalOffset.center,
            child: Container(
                margin: const EdgeInsets.symmetric(vertical: 4),
                child: Text(
                  DateFormat.MMMM().format(time),
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: ThemeProvider.fontBold,
                      fontSize: ThemeProvider.fontSize14),
                )),
          ),
        ),
      ),
    );
  }

  Widget _buildButtonBottom() {
    return Obx(() {
      return Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            decoration: const BoxDecoration(
                color: ThemeProvider.colorBackgroundScreen,
                border: Border(
                    top: BorderSide(
                        color: ThemeProvider.bgButtonFill, width: 0.5))),
            width: Get.width,
            height: 80,
            child: Row(
              children: [
                Expanded(
                    child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 24.w, vertical: 16.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${controller.bookingModel.value?.listProduct?.length} ${'services'.tr}',
                              style: TextStyle(
                                  fontFamily: ThemeProvider.fontMedium,
                                  fontSize: ThemeProvider.fontSize16))
                          .marginOnly(bottom: 5.h),
                      Text('${controller.bookingModel.value?.price} \$',
                          style: TextStyle(
                              fontFamily: ThemeProvider.fontRegular,
                              fontSize: ThemeProvider.fontSize14))
                    ],
                  ),
                )),
                Expanded(
                    child: InkWell(
                  onTap: () {
                    controller.order();
                  },
                  child: Container(
                    color: Colors.red,
                    child: Center(
                        child: Text(
                      'order'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize16,
                          color: ThemeProvider.colorWhite),
                    )),
                  ),
                ))
              ],
            ),
          ));
    });
  }

  Widget _buildSelectEmployee() {
    return Obx(
      () => BaseDropdownButton(
          borderRadiusButton: 6,
          buttonWidth: Get.width,
          dropdownWidth: Get.width - 16.w * 2,
          dropdownElevation: 2,
          hint: 'Chọn Nhân Viên',
          value: controller.employeeSelected.value,
          leadingHint: const Icon(
            Icons.person_outlined,
            color: ThemeProvider.colorHintText,
            size: 24,
          ).marginOnly(right: 5.w),
          icon: const Icon(
            Icons.keyboard_arrow_down_outlined,
            size: 24,
          ),
          dropdownItems: const ['Lili', 'Tina', 'Linda'],
          onChanged: (value) {
            controller.employeeSelected.value = value;
          }).marginOnly(right: 16.w),
    );
  }

  // Widget _buildListTime() {
  //   return Obx(
  //     () => ListView.builder(
  //       itemCount: controller.listTimeSelect.length,
  //       physics: const NeverScrollableScrollPhysics(),
  //       shrinkWrap: true,
  //       itemBuilder: (context, index) {
  //         return buildItem(controller.listTimeSelect[index]);
  //       },
  //     ).marginOnly(right: 16.w),
  //   );
  // }

  // Widget buildItem(TimeOrderModel itemTime) {
  //   return Row(children: [
  //     Expanded(flex: 3, child: Text(itemTime.time ?? '')),
  //     Expanded(
  //       flex: 1,
  //       child: BaseButton(
  //           borderColor: itemTime.isSelected
  //               ? ThemeProvider.bgButtonFillDisable
  //               : ThemeProvider.colorSelect,
  //           height: 30,
  //           textStyle: TextStyle(
  //               fontFamily: ThemeProvider.fontRegular,
  //               fontSize: ThemeProvider.fontSize12),
  //           title: itemTime.isSelected ? 'un_order'.tr : 'order'.tr,
  //           onPressed: () {
  //             itemTime.isSelected = !itemTime.isSelected;
  //             for (var element in controller.listTimeSelect) {
  //               if (element.id != itemTime.id) {
  //                 element.isSelected = false;
  //               }
  //             }
  //             controller.listTimeSelect.refresh();
  //             controller.seletedTime.value =
  //                 itemTime.isSelected ? itemTime : null;
  //           },
  //           radius: 8,
  //           styleButton: BaseButtonStyle.outline),
  //     )
  //   ]).marginOnly(bottom: 10.h);
  // }
}
