import 'package:json_annotation/json_annotation.dart';

part 'base_list_model.g.dart';

@JsonSerializable(
  genericArgumentFactories: true,
  createToJson: false,
)
class BaseListModel<T> {
  final bool? success;
  final String? message;
  final ListModel<T>? data;
  final int? status;

  BaseListModel({this.success, this.message, this.data, this.status});

  factory BaseListModel.fromJson(
          Map<String, dynamic> map, T Function(dynamic) fromJsonT) =>
      _$BaseListModelFromJson(map, fromJsonT);
}

@JsonSerializable(
  genericArgumentFactories: true,
  createToJson: false,
)
class ListModel<T> {
  @JsonKey(name: 'total_page')
  int? totalPage;
  int? page;
  @JsonKey(name: 'total')
  int? totalItem;
  List<T>? items;
  ListModel({this.totalPage, this.totalItem, this.items, this.page});

  factory ListModel.fromJson(
          Map<String, dynamic> map, T Function(dynamic) fromJsonT) =>
      _$ListModelFromJson(map, fromJsonT);
}
