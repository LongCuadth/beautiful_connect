import 'package:get/get.dart';

import '../../modules/splash/bindings/splash_binding.dart';
import '../../modules/splash/views/splash_view.dart';
import '../modules/forgot_password/bindings/forgot_password_binding.dart';
import '../modules/forgot_password/views/forgot_password_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/main_view/bindings/main_view_binding.dart';
import '../modules/main_view/views/main_view.dart';
import '../modules/order_final/bindings/order_final_binding.dart';
import '../modules/order_final/views/order_final_view.dart';
import '../modules/register/bindings/register_binding.dart';
import '../modules/register/views/register_view.dart';
import '../modules/select_step_time/bindings/select_step_time_binding.dart';
import '../modules/select_step_time/views/select_step_time_view.dart';

// ignore_for_file: constant_identifier_names

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.SPLASH,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => const RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.FORGOT_PASSWORD,
      page: () => const ForgotPasswordView(),
      binding: ForgotPasswordBinding(),
    ),
    GetPage(
      name: _Paths.MAIN_VIEW,
      page: () => const MainView(),
      binding: MainViewBinding(),
    ),
    GetPage(
      name: _Paths.SELECT_STEP_TIME,
      page: () => const SelectStepTimeView(),
      binding: SelectStepTimeBinding(),
    ),
    GetPage(
      name: _Paths.ORDER_FINAL,
      page: () => const OrderFinalView(),
      binding: OrderFinalBinding(),
    ),
  ];
}
