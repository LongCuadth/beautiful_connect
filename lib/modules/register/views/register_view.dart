import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/icon_circle_widget.dart';
import 'package:app/utils/validator_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  const RegisterView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        body: SafeArea(
            child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 16.w),
          child: Form(
            key: controller.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: IconCircleWidget(
                    padding: EdgeInsets.only(left: 8.w),
                    backgroundColor:
                        ThemeProvider.colorPrimary.withOpacity(0.2),
                    child: const Icon(
                      Icons.arrow_back_ios,
                      color: ThemeProvider.colorBlack,
                      size: 22,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    'app_name'.tr,
                    style: TextStyle(
                        fontFamily: ThemeProvider.fontLogoBold,
                        fontSize: ThemeProvider.fontSize28),
                  ),
                ),
                SizedBox(
                  width: Get.width,
                  height: 80.h,
                ),
                BaseTextField(
                    validator: (value) {
                      return ValidatorUtils.validatorEmail(value,
                          isRequired: true);
                    },
                    inputType: TextInputType.text,
                    textEditingController: controller.emailController,
                    hintText: 'email'.tr),
                buildFormUserInfo(),
                SizedBox(height: 50.h),
                Obx(() => BaseButton(
                    radius: 4,
                    title: controller.isSendedEmail.value
                        ? 'register'.tr
                        : 'confirm'.tr,
                    onPressed: () {
                      if (controller.formKey.currentState?.validate() == true) {
                        controller.registerUser();
                      }
                    },
                    styleButton: BaseButtonStyle.fill))
              ],
            ),
          ),
        )));
  }

  Widget buildFormUserInfo() {
    return Column(
      children: [
        SizedBox(height: 25.h),
        BaseTextField(
          textEditingController: controller.nameController,
          hintText: 'name'.tr,
          validator: (value) {
            if (controller.nameController.value.text.isEmpty) {
              return 'empty_name'.tr;
            }
            if (!RegExp(r'^.{6,}$')
                .hasMatch(controller.nameController.value.text)) {
              return 'invalid_name'.tr;
            }
            return null;
          },
        ),
        SizedBox(height: 25.h),
        BaseTextField(
          textEditingController: controller.phoneController,
          hintText: 'phone'.tr,
          validator: (value) {
            return ValidatorUtils.validatorPhone(value);
          },
          inputFormatters: [
            LengthLimitingTextInputFormatter(12),
          ],
          inputType: TextInputType.phone,
        ),
        SizedBox(height: 25.h),
        BaseTextField(
          isPassword: true,
          textEditingController: controller.passController,
          hintText: 'password'.tr,
          validator: (value) {
            return ValidatorUtils.validatorPass(controller.passController.text);
          },
        ),
        SizedBox(height: 25.h),
        BaseTextField(
          isPassword: true,
          textEditingController: controller.checkPassController,
          hintText: 'password_check'.tr,
          validator: (value) {
            return ValidatorUtils.validatorRePass(
                controller.passController.text,
                controller.checkPassController.text);
          },
        ),
        SizedBox(height: 25.h),
        BaseTextField(
          inputType: TextInputType.text,
          textEditingController: controller.inviteController,
          hintText: 'code_shop'.tr,
          validator: (value) {
            if (controller.inviteController.value.text.isEmpty) {
              return 'invalide_code_shop'.tr;
            }
            return null;
          },
        ),
      ],
    );
  }
}
