import 'package:app/base/widgets/base_button.dart';
import 'package:app/data/models/category_model.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/modules/main_view/views/menu_drawer_view.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/date_time_format.dart';
import 'package:app/utils/icon_circle_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:badges/badges.dart' as badge;
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import '../controllers/main_view_controller.dart';

class MainView extends GetView<MainViewController> {
  const MainView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MyDrawerController>(builder: (drawerController) {
      return ZoomDrawer(
        menuScreen: MenuDrawerView(drawerController: drawerController),
        controller: drawerController.zoomDrawerController,
        mainScreen: _buildMainView(drawerController),
        showShadow: false,
        angle: 0,
        mainScreenScale: 0.2,
        borderRadius: 15,
        menuBackgroundColor: ThemeProvider.colorWhite.withOpacity(0.05),
        moveMenuScreen: false,
        menuScreenWidth: MediaQuery.of(context).size.width,
        slideWidth: MediaQuery.of(context).size.width * 0.65,
      );
    });
  }

  Scaffold _buildMainView(MyDrawerController drawerController) {
    return Scaffold(
      key: controller.scaffoldKey,
      drawerEnableOpenDragGesture: false,
      backgroundColor: ThemeProvider.colorBackgroundScreen,
      appBar: AppBar(
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            drawerController.toggleDrawer();
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Assets.svg.icMenuBar
                .svg(
                    width: 24.w,
                    colorFilter: const ColorFilter.mode(
                        ThemeProvider.colorBlack, BlendMode.srcIn))
                .marginOnly(left: 16.w),
          ),
        ),
        title: Text(
          'app_name'.tr,
          style: TextStyle(
              color: ThemeProvider.colorBlack,
              fontFamily: ThemeProvider.fontLogoBold,
              fontSize: ThemeProvider.fontSize28),
        ),
        centerTitle: true,
        actions: [
          Obx(
            () => controller.notiUnreadCount.value > 0
                ? Center(
                    child: badge.Badge(
                      position: badge.BadgePosition.custom(
                          end: 12, bottom: 10, start: 0),
                      badgeContent: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(2),
                      ),
                      badgeStyle: const badge.BadgeStyle(
                        badgeColor: ThemeProvider.colorUnreadNoti,
                        padding: EdgeInsets.all(6),
                      ),
                      child: Assets.svg.icNotification.svg(width: 24.w),
                    ).marginOnly(right: 16.w),
                  )
                : Assets.svg.icNotification.svg().marginOnly(right: 16.w),
          ),
        ],
      ),
      body: Stack(
        children: [
          SizedBox(
            width: Get.width,
            height: Get.height,
            child: RefreshIndicator(
              onRefresh: () async {
                controller.onRefresh();
              },
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                padding: EdgeInsets.only(left: 16.w),
                child: Column(
                  children: [
                    _buildBanner(),
                    SizedBox(height: 20.h),
                    _buildListCategory(),
                    _buildDetailServices(),
                    SizedBox(height: 200.h),
                  ],
                ),
              ),
            ),
          ),
          _buildButtonBottom()
        ],
      ),
    );
  }

  Widget _buildButtonBottom() {
    return Obx(() {
      return Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Visibility(
            visible: controller.listServicesSelect.isNotEmpty,
            child: Container(
              decoration: const BoxDecoration(
                  color: ThemeProvider.colorBackgroundScreen,
                  border: Border(
                      top: BorderSide(
                          color: ThemeProvider.bgButtonFill, width: 0.5))),
              width: Get.width,
              height: 80,
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 24.w, vertical: 16.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('${controller.listServicesSelect.length} ${'services'.tr}',
                                style: TextStyle(
                                    fontFamily: ThemeProvider.fontMedium,
                                    fontSize: ThemeProvider.fontSize16))
                            .marginOnly(bottom: 5.h),
                        Text('${controller.price} €',
                            style: TextStyle(
                                fontFamily: ThemeProvider.fontRegular,
                                fontSize: ThemeProvider.fontSize14))
                      ],
                    ),
                  )),
                  Expanded(
                      child: InkWell(
                    onTap: () {
                      controller.nextStepBooking();
                    },
                    child: Container(
                      color: Colors.red,
                      child: Center(
                          child: Text(
                        'order'.tr,
                        style: TextStyle(
                            fontFamily: ThemeProvider.fontMedium,
                            fontSize: ThemeProvider.fontSize16,
                            color: ThemeProvider.colorWhite),
                      )),
                    ),
                  ))
                ],
              ),
            ),
          ));
    });
  }

  _buildListCategory() {
    return Obx(() => SizedBox(
        height: 100.h,
        child: ListView.builder(
          itemCount: controller.listCategory.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return _buildItemCategory(controller.listCategory[index])
                .marginSymmetric(horizontal: 8, vertical: 16);
          },
        )));
  }

  Widget _buildItemCategory(CategoryModel item) {
    return GestureDetector(
      onTap: () {
        controller.onSelectCategory(item);
      },
      child: Container(
        width: 150.w,
        padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.h),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: ThemeProvider.colorSelect.withOpacity(0.2),
              blurRadius: 10,
              offset: const Offset(1, 1), // changes position of shadow
            ),
          ],
          // border: Border.all(color: ThemeProvider.colorSelect),
          borderRadius: BorderRadius.circular(10),
          color: ThemeProvider.colorWhite,
        ),
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Flexible(
              child: Text(
            item.name ?? '',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontFamily: ThemeProvider.fontSemiBold,
                fontSize: ThemeProvider.fontSize14),
          )),
          Visibility(
            visible: item.isSelected,
            child: IconCircleWidget(
              width: 20.w,
              height: 20.w,
              padding: EdgeInsets.zero,
              backgroundColor: ThemeProvider.colorSelect,
              child: Assets.svg.icSelectServices.svg(
                width: 16.w,
                height: 16.w,
              ),
            ),
          ).marginOnly(left: 10.w)
        ]),
      ),
    );
  }

  Widget _buildDetailServices() {
    return GetBuilder<MainViewController>(
        builder: (controller) => controller.categorySelect.value?.id == null
            ? Container(
                height: Get.height / 2,
                margin: EdgeInsets.only(right: 16.w),
                child: Center(
                  child: Text(
                    'Mời bạn chọn dịch vụ',
                    style: TextStyle(
                        fontFamily: ThemeProvider.fontSemiBold,
                        fontSize: ThemeProvider.fontSize16),
                  ),
                ),
              )
            : controller.listServisesShow.isNotEmpty
                ? SizedBox(
                    child: ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      separatorBuilder: (context, index) {
                        return const Divider(
                          color: ThemeProvider.colorBlack,
                          thickness: 0.2,
                        );
                      },
                      itemCount: controller.listServisesShow.length,
                      itemBuilder: (context, index) {
                        return _buildServices(index);
                      },
                    ),
                  ).marginOnly(bottom: 20.h, right: 16.w)
                : Container(
                    height: Get.height / 2,
                    margin: EdgeInsets.only(right: 16.w),
                    child: Center(
                      child: Text(
                        'coming_soon'.tr,
                        style: TextStyle(
                            fontFamily: ThemeProvider.fontSemiBold,
                            fontSize: ThemeProvider.fontSize16),
                      ),
                    ),
                  ));
  }

  Widget _buildServices(int index) {
    var itemService = controller.listServisesShow[index];
    return ExpandableNotifier(
      initialExpanded: true,
      child: ExpandablePanel(
          key: UniqueKey(),
          theme: const ExpandableThemeData(
              headerAlignment: ExpandablePanelHeaderAlignment.center,
              hasIcon: true,
              tapHeaderToExpand: true),
          header: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                itemService.name ?? '',
                style: TextStyle(
                    fontFamily: ThemeProvider.fontSemiBold,
                    fontSize: ThemeProvider.fontSize16),
              ),
            ],
          ),
          collapsed: const SizedBox.shrink(),
          expanded: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'info_services'.tr,
                style: TextStyle(
                    color: const Color(0xff0C58B1),
                    fontFamily: ThemeProvider.fontMedium,
                    fontSize: ThemeProvider.fontSize14),
              ),
              SizedBox(
                height: 12.h,
              ),
              Visibility(
                visible: itemService.description?.isNotEmpty == true,
                child: Text(
                  itemService.description ?? '',
                  style: TextStyle(
                      color: ThemeProvider.colorBlack,
                      fontFamily: ThemeProvider.fontRegular,
                      fontSize: ThemeProvider.fontSize14),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: Text(
                      '${itemService.price?.toString() ?? ''} € / ${formatMinuteToString(itemService.duration?.toDouble() ?? 0.0)} ',
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontRegular,
                          fontSize: ThemeProvider.fontSize14),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: BaseButton(
                            borderColor: itemService.isBooked
                                ? ThemeProvider.bgButtonFillDisable
                                : ThemeProvider.colorSelect,
                            height: 30,
                            textStyle: TextStyle(
                                fontFamily: ThemeProvider.fontRegular,
                                fontSize: ThemeProvider.fontSize12),
                            title: itemService.isBooked
                                ? 'un_order'.tr
                                : 'order'.tr,
                            onPressed: () {
                              controller.onSelectServices(itemService);
                            },
                            radius: 8,
                            styleButton: BaseButtonStyle.outline)
                        .paddingSymmetric(horizontal: 8),
                  )
                ],
              ),
            ],
          )),
    );
  }

  _buildBanner() {
    return controller.listBanner.isEmpty
        ? const SizedBox()
        : ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 16.w),
              width: Get.width,
              child: Stack(alignment: Alignment.bottomCenter, children: [
                Obx(
                  () => CarouselSlider.builder(
                    itemCount: controller.listBanner.length,
                    itemBuilder: (context, index, _) {
                      return GestureDetector(
                        onTap: () {},
                        child: SizedBox(
                          child: ClipRRect(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10.0)),
                              child: CachedNetworkImage(
                                imageUrl:
                                    controller.listBanner[index].image ?? '',
                                fit: BoxFit.cover,
                                width: double.maxFinite,
                              )),
                        ),
                      );
                    },
                    carouselController: controller.carouselController,
                    options: CarouselOptions(
                        autoPlay: true,
                        enlargeCenterPage: true,
                        viewportFraction: 1.0,
                        aspectRatio: 2.42,
                        onPageChanged: (index, reason) {
                          controller.currentIndexCarousel.value = index;
                        }),
                  ),
                ),
                Obx(
                  () => Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: controller.listBanner
                        .toList()
                        .asMap()
                        .entries
                        .map((entry) {
                      return GestureDetector(
                        onTap: () => controller.carouselController
                            .animateToPage(entry.key),
                        child: Container(
                          width: 8.0,
                          height: 8.0,
                          margin: const EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: ThemeProvider.colorWhite.withOpacity(
                                  controller.currentIndexCarousel.value ==
                                          entry.key
                                      ? 0.9
                                      : 0.5)),
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ]),
            ),
          );
  }
}
