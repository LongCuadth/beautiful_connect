import 'package:app/data/providers/banner/banner_provider.dart';
import 'package:app/data/providers/category/category_provider.dart';
import 'package:app/data/providers/service/service_provider.dart';
import 'package:get/get.dart';

import '../controllers/main_view_controller.dart';

class MainViewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IServicesProvider>(
      () => ServicesProvider(),
      fenix: true,
    );
    Get.lazyPut<ICategoryProvider>(
      () => CategoryProvider(),
      fenix: true,
    );
    Get.lazyPut<IBannerProvider>(
      () => BannerProvider(),
      fenix: true,
    );

    Get.put<MyDrawerController>(MyDrawerController());
    Get.lazyPut<MainViewController>(
      () => MainViewController(),
    );
  }
}
