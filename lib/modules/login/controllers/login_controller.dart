import 'package:app/common/app_parameters.dart';
import 'package:app/common/network/shared_preference.dart';
import 'package:app/data/providers/auth/auth_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/routes/app_pages.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  var emailController =
      TextEditingController(text: 'longtest@gmail.com'); //longtest@gmail.com
  var passController = TextEditingController(text: '12345678'); //12345678
  var keyForm = GlobalKey<FormState>();
  final IAuthProvider _authProvider = Get.find<IAuthProvider>();
  final MessageService _messageService = Get.find<MessageService>();

  void login() async {
    if (keyForm.currentState!.validate()) {
      EasyLoading.show();
      final resToken = await _authProvider.login({
        AppParameters.password: passController.text,
        AppParameters.username: emailController.text
      });
      EasyLoading.dismiss();
      if (resToken.success == true && resToken.data != null) {
        await SharedPreference.shared
            .saveToken(resToken.data?.accessToken ?? '');
        final resUserInfo = await _authProvider.getUserInfo();
        if (resUserInfo.success == true &&
            resUserInfo.data != null &&
            resUserInfo.data!.shop != null) {
          _messageService.send(Message.success(content: 'login_success'.tr));
          // Save User & Token
          await SharedPreference.shared.saveShopModel(resUserInfo.data!.shop!);
          Get.offAndToNamed(Routes.MAIN_VIEW);
        }
      } else {
        if (resToken.message?.contains('Unauthorized') == true) {
          _messageService
              .send(Message.error(content: 'wrong_login_account'.tr));
        }
      }
    }
  }
}
