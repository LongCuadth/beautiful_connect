import 'package:app/data/models/banner_model.dart';
import 'package:app/data/models/category_model.dart';
import 'package:app/data/models/services_model.dart';
import 'package:app/data/providers/banner/banner_provider.dart';
import 'package:app/data/providers/category/category_provider.dart';
import 'package:app/data/providers/service/service_provider.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';

class MainViewController extends GetxController {
  var notiUnreadCount = 3.obs;
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var listServices = <ServicesModel>[];
  var listServisesShow = <ServicesModel>[].obs;
  var listServicesSelect = <ServicesModel>[].obs;
  var listCategory = <CategoryModel>[].obs;
  var categorySelect = Rx<CategoryModel?>(null);
  var price = 0.0.obs;
  var carouselController = CarouselController();
  var currentIndexCarousel = 0.obs;
  var listBanner = <BannerModel>[].obs;
  final IServicesProvider _serviceProvider = Get.find<IServicesProvider>();
  final ICategoryProvider _categoryProvider = Get.find<ICategoryProvider>();
  final IBannerProvider _bannerProvider = Get.find<IBannerProvider>();

  @override
  void onInit() {
    getData();
    super.onInit();
  }

  Future<void> getData() async {
    EasyLoading.show();
    var res = await _serviceProvider.getListService({
      'all': 1,
    });
    if (res.success == true && res.data!.items != null) {
      listServices = res.data!.items!;
    }
    var res1 = await _categoryProvider.getListCategory({
      'all': 1,
    });
    if (res1.success == true && res1.data != null) {
      listCategory.value = res1.data!.items!;
    }
    var res2 = await _bannerProvider.getListBanner({'size': 1000});
    if (res2.success == true && res2.data != null) {
      listBanner.value = res2.data!.items!;
    }
    EasyLoading.dismiss();
  }

  void onSelectCategory(CategoryModel item) {
    for (var element in listCategory) {
      if (element.id != item.id) {
        element.isSelected = false;
      } else {
        element.isSelected = true;
      }
    }
    categorySelect.value = item;
    listCategory.refresh();
    listServisesShow.clear();
    listServisesShow
        .addAll(listServices.where((element) => element.idCategory == item.id));
    update();
  }

  void nextStepBooking() {
    // var bookingModel = BookingModel(
    //     servicesModel: listServicesSelect.toList(),
    //     price: price.value.toString());
    // Get.toNamed(Routes.SELECT_STEP_TIME, arguments: {
    //   AppParameters.bookingModel: bookingModel,
    // });
  }

  void onSelectServices(ServicesModel itemService) {
    itemService.isBooked = !itemService.isBooked;
    update();
    itemService.isBooked
        ? listServicesSelect.add(itemService)
        : listServicesSelect.removeWhere((item) => item.id == itemService.id);
    var price = 0.0;
    for (var element in listServicesSelect) {
      price += double.parse(element.price ?? '0');
    }
    this.price.value = price;
  }

  Future<void> onRefresh() async {
    listServices.clear();
    listCategory.clear();
    listServisesShow.clear();
    await getData();
    for (var element in listCategory) {
      if (element.id == categorySelect.value?.id) {
        element.isSelected = true;
      } else {
        element.isSelected = false;
      }
    }
    for (var item in listServices) {
      for (var itemSelect in listServicesSelect) {
        if (item.id == itemSelect.id) {
          item.isBooked = true;
        }
      }
    }
    onSelectCategory(categorySelect.value!);
  }
}

class MyDrawerController extends GetxController {
  final zoomDrawerController = ZoomDrawerController();

  void toggleDrawer() {
    zoomDrawerController.toggle?.call();
    update();
  }
}
