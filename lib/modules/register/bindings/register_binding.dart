import 'package:app/data/providers/auth/auth_provider.dart';
import 'package:get/get.dart';

import '../controllers/register_controller.dart';

class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IAuthProvider>(() => AuthProvider());
    Get.lazyPut<RegisterController>(
      () => RegisterController(),
    );
  }
}
